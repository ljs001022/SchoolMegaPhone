package daelim.schoolmegaphone.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRequestDto {
    private String uid;
    private String pass;
    private String name;
    private Long role;
}
