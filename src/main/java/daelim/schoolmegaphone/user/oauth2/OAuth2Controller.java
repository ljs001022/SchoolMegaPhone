package daelim.schoolmegaphone.user.oauth2;

import daelim.schoolmegaphone.config.res.ResponseDTO;
import daelim.schoolmegaphone.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/oauth")
@RestController
@Slf4j
public class OAuth2Controller {
    private final OAuth2Service oAuthService;
    private final UserService userService;
    public OAuth2Controller(OAuth2Service oAuthService, UserService userService) {
        this.oAuthService = oAuthService;
        this.userService = userService;
    }
    @RequestMapping(value = "/kakao", method = RequestMethod.POST)
    public ResponseDTO<?> kakaoCallBack(@RequestParam String code) throws Exception {
        String token = oAuthService.getKakaoAccessToken(code);
        KakaoUserInfo kakaoUserInfo = oAuthService.getKakaoUser(token);
        return userService.kakaoSign(kakaoUserInfo);
    }
    @RequestMapping(value = "/naver", method = RequestMethod.GET)
    public ResponseDTO<?> naverCallBack(@RequestParam String code) throws Exception {
        String token = oAuthService.getNaverAccessToken(code);
        NaverUserInfo naverUserInfo = oAuthService.getNaverUser(token);
        return userService.naverSign(naverUserInfo);
    }
}
