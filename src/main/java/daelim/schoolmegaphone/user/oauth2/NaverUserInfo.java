package daelim.schoolmegaphone.user.oauth2;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NaverUserInfo {
    private String id;
    private String email;
    private String nickname;
}
