package daelim.schoolmegaphone.comment.repository;

import daelim.schoolmegaphone.board.Board;
import daelim.schoolmegaphone.comment.Comment;

import java.util.List;
import java.util.Optional;

public interface CommentRepositoryCustom {
    List<Comment> findAllByBoard(Board board);
    Optional<Comment> findCommentByIdWithParent(Long id);
}