package daelim.schoolmegaphone.comment.dto;

import daelim.schoolmegaphone.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommentRequestDto {
	private Long id;
	private String content;
	private User userId;
	private Long boardId;
	private Long parentId;
}
