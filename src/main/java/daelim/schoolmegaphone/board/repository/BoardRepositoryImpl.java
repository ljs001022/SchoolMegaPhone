package daelim.schoolmegaphone.board.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import daelim.schoolmegaphone.board.QBoard;
import daelim.schoolmegaphone.board.dto.BoardResponseDto;
import daelim.schoolmegaphone.board.dto.QBoardResponseDto;
import daelim.schoolmegaphone.user.QUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BoardRepositoryImpl implements BoardRepositoryCustom {
    private final JPAQueryFactory jpaQueryFactory;

    public BoardRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

    @Override
    public Slice<BoardResponseDto> getBoardSlice(Pageable pageable) {
        QBoard board = QBoard.board;
        QUser user = QUser.user;
        List<BoardResponseDto> dtoList = jpaQueryFactory.select(new QBoardResponseDto(board, user))
                .from(board)
                .innerJoin(user)
                .on(board.user.eq(user))
                .where(board.state.eq(1L))
                .where(board.isDeleted.eq(0L))
                .orderBy(board.createDate.desc())
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize()+1)
                .fetch();

        boolean hasNext = false;
        if(dtoList.size() > pageable.getPageSize()) {
            dtoList.remove(pageable.getPageSize());
            hasNext =true;
        }

        return new SliceImpl<>(dtoList, pageable, hasNext);
    }
}
