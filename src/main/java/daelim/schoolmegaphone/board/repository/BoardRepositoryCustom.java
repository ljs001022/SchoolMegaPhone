package daelim.schoolmegaphone.board.repository;

import daelim.schoolmegaphone.board.dto.BoardResponseDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

public interface BoardRepositoryCustom {
    Slice<BoardResponseDto> getBoardSlice(Pageable pageable);
}
