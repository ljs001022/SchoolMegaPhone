package daelim.schoolmegaphone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class SchoolMegaPhoneSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolMegaPhoneSpringApplication.class, args);
	}

}
