package daelim.schoolmegaphone;

import daelim.schoolmegaphone.user.User;
import daelim.schoolmegaphone.user.UserRepository;
import daelim.schoolmegaphone.user.UserService;
import daelim.schoolmegaphone.user.dto.UserRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MakeInitData {
    private final UserService userService;
    private final PasswordEncoder encoder;
    @PostConstruct
    public void makeAdmin() {
        Optional<User> user = userService.validate("superadmin");
        if(user.isEmpty()){
            UserRequestDto admin = UserRequestDto.builder()
                    .uid("superadmin")
                    .pass(encoder.encode("rhkdwnwnd2023"))
                    .role(1L)
                    .name("관리자")
                    .build();
            userService.register(admin);
        }
    }
}
